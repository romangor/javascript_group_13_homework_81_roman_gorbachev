import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
  changeURL = new Subject<string>();

  shortUrl!: string;

  constructor(private http: HttpClient) {
  }

  sendUrl(urlUser: string, symbolNumber: number){
    const body =  {
      userLink: urlUser,
      symbolNumber: symbolNumber
    }
    this.http.post(env.apiUrl + '/links', body).subscribe(response => {
      const data = Object.values(response)[1];
      this.shortUrl = `http://localhost:8000/links/${data}`;
      this.changeURL.next(this.shortUrl);
    })
  };
}
