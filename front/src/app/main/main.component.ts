import { Component, OnInit } from '@angular/core';
import { UrlService } from './url.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {
  userUrl!: string;
  shortUrl!: string;
  symbolNumber!: number;

  constructor(private urlService: UrlService) { }

  ngOnInit(): void {
    this.urlService.changeURL.subscribe( url => {
      this.shortUrl = url;
    })
  }

  sendUrl() {
    this.urlService.sendUrl(this.userUrl, this.symbolNumber);
  }

}
