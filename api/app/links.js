const express = require('express');
const Link = require("./models/Link");
const {nanoid} = require("nanoid");
const router = express.Router();

router.post('/', async (req, res, next) => {
    try {
        const shortLink = nanoid(req.body.symbolNumber);
        const data = {
            link: req.body.userLink,
            shortLink: shortLink
        }
        const linkData = new Link(data);
        await linkData.save();
        res.send(linkData);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const result = await Link.find({shortLink: req.params.id});
        if (result.length > 0) {
            res.status(301).redirect(result[0].link);
        }
        res.status(404).send({message: 'Url not found'});
    } catch (e) {
        next(e);
    }
});

module.exports = router;