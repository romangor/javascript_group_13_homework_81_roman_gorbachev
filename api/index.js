const express = require('express');
const mongoose = require("mongoose");
const cors = require('cors');
const links = require('./app/links');

const app = express();

const port = 8000;

app.use(cors());
app.use(express.json());
app.use('/links', links);

const run = async () => {
    await mongoose.connect('mongodb://localhost/shortLink', {useNewUrlParser: true});

    app.listen(port, () => {
        console.log(` Server started at ${port} port`);
    });

    process.on('exit', () => {
        mongoose.disconnect();
    });
}

run().catch(e => console.error(e));